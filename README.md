#### Требования к Software
- JRE 1.7
#### Стек Технологий
- Java SE 1.7
- Maven
#### Контактные данные разработчика
- bahrsshumov@gmail.com
#### Команды для сборки
```
mvn clean install
```
#### Команды для запуска приложения
```
java -jar ./<.jar file>
```