package ru.shumov.tm;

import ru.shumov.tm.entity.Project;
import ru.shumov.tm.entity.Task;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;


public class CreateTask {

    private Map<String, Project> projects;
    private Map<String, Task> tasks;

    public CreateTask(Map<String, Task> tasks, Map<String, Project> projects) {
        this.tasks = tasks;
        this.projects = projects;
    }

    public void createTask() throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat();
        format.applyPattern("dd.MM.yyyy");
        Scanner scrTasks = new Scanner(System.in);
        System.out.println(Commands.ENTER_PROJECT_ID_FOR_TASKS);
        String projectId = scrTasks.nextLine();
        if (projects.containsKey(projectId)) {
            System.out.println(Commands.ENTER_TASK_NAME);
            String name = scrTasks.nextLine();
            Task task = new Task();
            System.out.println(Commands.ENTER_START_DATE_OF_TASK);
            String startDateS = scrTasks.nextLine();
            Date StartDateD = format.parse(startDateS);
            System.out.println(Commands.ENTER_DEADLINE_OF_TASK);
            String endDateS = scrTasks.nextLine();
            Date endDateD = format.parse(endDateS);
            String id = UUID.randomUUID().toString();
            System.out.println(Commands.ENTER_DESCRIPTION_OF_TASK);
            String description = scrTasks.nextLine();

            task.setDescription(description);
            task.setEndDate(endDateD);
            task.setStartDate(StartDateD);
            task.setProjectId(projectId);
            task.setName(name);
            task.setId(id);

            tasks.put(id, task);
            System.out.println(Commands.DONE);
        } else {
            System.out.println(Commands.PROJECT_DOES_NOT_EXIST);
        }

    }
}
