package ru.shumov.tm;

import ru.shumov.tm.entity.Project;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Scanner;

public class ProjectUpdate {

    private Map<String, Project> projects;

    public ProjectUpdate(Map<String, Project> projects) {
        this.projects = projects;
    }

    public void projectUpdate() throws ParseException {
        Scanner scrUpdate = new Scanner(System.in);
        SimpleDateFormat format = new SimpleDateFormat();
        format.applyPattern("dd.MM.yyyy");
        System.out.println("Введите id проекта который хотите изменить.");
        String projectId = scrUpdate.nextLine();
        if(projects.containsKey(projectId)){
            System.out.println();
            String name = scrUpdate.nextLine();
            System.out.println();
            String description = scrUpdate.nextLine();
            System.out.println();
            String startDateS = scrUpdate.nextLine();
            System.out.println();
            String endDateS = scrUpdate.nextLine();
            Project project = projects.get(projectId);
            project.setName(name);
            project.setDescription(description);
            project.setStartDate(format.parse(startDateS));
            project.setEndDate(format.parse(endDateS));
            System.out.println(Commands.DONE);
        }
    }
}
