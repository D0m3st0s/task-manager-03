package ru.shumov.tm;

import ru.shumov.tm.entity.Project;
import ru.shumov.tm.entity.Task;

import java.util.Collection;
import java.util.Map;
import java.util.Scanner;

public class ProjectRemove {
    private Map<String, Task> tasks;
    private Map<String, Project> projects;

    public ProjectRemove(Map<String, Task> tasks, Map<String, Project> projects) {
        this.tasks = tasks;
        this.projects = projects;
    }

    public void projectRemove() {
        Scanner scrProjectRe = new Scanner(System.in);
        System.out.println(Commands.ENTER_PROJECT_ID_FOR_REMOVING);
        String projectId = scrProjectRe.nextLine();
        if (projectId != null) {
            Project remove = projects.remove(projectId);
            if(remove != null){
                Collection<Task> values = tasks.values();
                for (Task task : values){
                    if(task.getProjectId().equals(projectId)){
                        tasks.remove(task.getId());
                    }
                }
            }
        }
        System.out.println(Commands.DONE);
    }
}
