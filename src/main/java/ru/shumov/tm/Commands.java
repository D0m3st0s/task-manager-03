package ru.shumov.tm;

public class Commands {

    final static String HELP = "help";
    final static String HELP_HELP = "help : Вывод доступных команд.";
    final static String PROJECT_CREATE = "project create";
    final static String HELP_PROJECT_CREATE = "project create: Создание нового проекта.";
    final static String PROJECT_CLEAR = "project clear";
    final static String HELP_PROJECT_CLEAR = "project clear: Удаление всех проектов.";
    final static String PROJECT_REMOVE = "project remove";
    final static String HELP_PROJECT_REMOVE = "project remove: Выборочное удаление проектов.";
    final static String PROJECT_LIST = "project list";
    final static String HELP_PROJECT_LIST = "project list: Вывод всех проектов.";
    final static String TASK_CREATE = "task create";
    final static String HELP_TASK_CREATE = "task create: Создание нового задания.";
    final static String TASK_CLEAR = "task clear";
    final static String HELP_TASK_CLEAR = "task clear: Удаление всех задач.";
    final static String TASK_REMOVE = "task remove";
    final static String HELP_TASK_REMOVE = "task remove: Выборочное удаление задач.";
    final static String TASK_LIST = "task list";
    final static String HELP_TASK_LIST = "task list: Вывод всех задач.";
    final static String EXIT = "exit";
    final static String HELP_EXIT = "exit: Остановка программы.";

    final static String WELCOME = "       Добро Пожаловать в Task Manager";
    final static String PROJECT_ID = "...ID Проекта Можно Найти в Списке Проектов...";

    final static String DONE = "Done";
    final static String YES_NO = "yes/no";
    final static String YES = "yes";
    final static String NO = "no";

    static String ENTER_PROJECT_NAME = "Введите имя проекта:";
    final static String ENTER_PROJECT_ID_FOR_TASKS = "Чтобы добавить задание к проекту введите id проекта:";
    final static String ENTER_PROJECT_ID_FOR_REMOVING = "Введите ID Проекта который вы хотите удалить:";
    final static String ENTER_PROJECT_ID_FOR_SHOWING_TASKS = "Введите id проекта задачи которого нужно вывести:";
    final static String ENTER_PROJECT_ID = "Введите id проекта который хотите вывести:";
    final static String ENTER_DEADLINE_OF_PROJECT = "Введите дату окончания работы над проектом:";
    final static String ENTER_START_DATE_OF_PROJECT = "Введите дату начала работы над проектом:";
    final static String ENTER_DESCRIPTION_OF_PROJECT = "Введите описание проекта:";
    final static String ENTER_TASK_NAME = "Введите назввание задачи:";
    final static String ENTER_TASK_ID_FOR_REMOVING = "Введите id задачи которую вы хотите удалить:";
    final static String ENTER_DEADLINE_OF_TASK = "Введите дату окончания работы над заданием:";
    final static String ENTER_START_DATE_OF_TASK = "Введите дату начала работы над заданием:";
    final static String ENTER_DESCRIPTION_OF_TASK = "Введите описание задания:";

    final static String PROJECT_DOES_NOT_EXIST = "Такого проекта нет.";
    final static String PROJECTS_DO_NOT_EXIST = "Проектов нет";
    final static String ALL_PROJECTS_WILL_BE_CLEARED = "Все проекты будут удалены, Вы уверны?";

    final static String SHOW_ALL_PROJECTS = "Вывести все проекты?";
    final static String ALL_TASKS_WILL_BE_CLEARED = "Все задачи будут удалены, Вы уверны?";
    final static String TASKS_DO_NOT_EXIST = "Задач нет.";
    final static String SHOW_ALL_TASKS = "Вывести все задачи?";
}
