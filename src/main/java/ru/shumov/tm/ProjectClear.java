package ru.shumov.tm;

import ru.shumov.tm.entity.Project;
import ru.shumov.tm.entity.Task;

import java.util.Map;
import java.util.Scanner;

public class ProjectClear {
    private Map<String, Task> tasks;
    private Map<String, Project> projects;

    public ProjectClear(Map<String, Task> tasks, Map<String, Project> projects) {
        this.tasks = tasks;
        this.projects = projects;
    }

    public void projectClear() {
        Scanner scrPrClear = new Scanner(System.in);
        System.out.println(Commands.ALL_PROJECTS_WILL_BE_CLEARED);
        System.out.println(Commands.YES_NO);
        String answer = scrPrClear.nextLine();

        if (answer.equals(Commands.YES)) {
            projects.clear();
            tasks.clear();
            System.out.println(Commands.DONE);
        }
    }
}
