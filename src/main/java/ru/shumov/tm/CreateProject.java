package ru.shumov.tm;


import ru.shumov.tm.entity.Project;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class CreateProject {

    private Map<String, Project> projects;

    public CreateProject(Map<String, Project> projects) {
        this.projects = projects;
    }

    public void createProject() throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat();
        format.applyPattern("dd.MM.yyyy");
        Scanner scrName = new Scanner(System.in);
        System.out.println(Commands.ENTER_PROJECT_NAME);
        String name = scrName.nextLine();

        Project project = new Project();
        String id = UUID.randomUUID().toString();
        System.out.println(Commands.ENTER_START_DATE_OF_PROJECT);
        String startDateS = scrName.nextLine();
        Date StartDateD = format.parse(startDateS);
        System.out.println(Commands.ENTER_DEADLINE_OF_PROJECT);
        String endDateS = scrName.nextLine();
        Date endDateD = format.parse(endDateS);
        System.out.println(Commands.ENTER_DESCRIPTION_OF_PROJECT);
        String description = scrName.nextLine();

        project.setDescription(description);
        project.setName(name);
        project.setStartDate(StartDateD);
        project.setEndDate(endDateD);
        project.setId(id);

        projects.put(id, project);
        System.out.println(Commands.DONE);
    }
}
