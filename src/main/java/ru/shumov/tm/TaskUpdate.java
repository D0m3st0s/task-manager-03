package ru.shumov.tm;

import ru.shumov.tm.entity.Project;
import ru.shumov.tm.entity.Task;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.Scanner;

public class TaskUpdate {

    private Map<String, Task> tasks;

    public TaskUpdate(Map<String, Task> tasks) {
        this.tasks = tasks;
    }

    public void taskUpdate() throws ParseException {
        Scanner scrUpdate = new Scanner(System.in);
        SimpleDateFormat format = new SimpleDateFormat();
        format.applyPattern("dd.MM.yyyy");
        System.out.println("Введите id задания который хотите изменить.");
        String taskId = scrUpdate.nextLine();
        if(tasks.containsKey(taskId)){
            System.out.println(Commands.ENTER_TASK_NAME);
            String name = scrUpdate.nextLine();
            System.out.println(Commands.ENTER_DESCRIPTION_OF_TASK);
            String description = scrUpdate.nextLine();
            System.out.println(Commands.ENTER_START_DATE_OF_TASK);
            String startDateS = scrUpdate.nextLine();
            System.out.println(Commands.ENTER_DEADLINE_OF_PROJECT);
            String endDateS = scrUpdate.nextLine();
            Task task = tasks.get(taskId);
            task.setName(name);
            task.setDescription(description);
            task.setStartDate(format.parse(startDateS));
            task.setEndDate(format.parse(endDateS));
            System.out.println(Commands.DONE);
        }
    }
}
