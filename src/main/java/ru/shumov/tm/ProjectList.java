package ru.shumov.tm;

import ru.shumov.tm.entity.Project;

import java.util.Collection;
import java.util.Map;
import java.util.Scanner;

public class ProjectList {

    private Map<String, Project> projects;

    public ProjectList(Map<String, Project> projects) {
        this.projects = projects;
    }

    public void projectList() {
        Scanner scrProjectList = new Scanner(System.in);
        if (projects.isEmpty()) {
            System.out.println(Commands.PROJECTS_DO_NOT_EXIST);
        } else {
            System.out.println(Commands.SHOW_ALL_PROJECTS);
            String answer = scrProjectList.nextLine();
            if(answer.equals(Commands.YES)){
                Collection<Project> values = projects.values();
                for(Project project : values){
                    System.out.println(project);
                }
            }
            if(answer.equals(Commands.NO)){
                System.out.println(Commands.ENTER_PROJECT_ID);
                String projectId = scrProjectList.nextLine();
                Collection<Project> values = projects.values();
                for (Project project : values){
                    if(project.getId().equals(projectId)){
                        System.out.println(project);
                    }
                }
            }
        }
    }
}
