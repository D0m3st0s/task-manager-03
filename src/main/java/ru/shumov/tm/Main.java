package ru.shumov.tm;

import ru.shumov.tm.entity.Project;
import ru.shumov.tm.entity.Task;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import static ru.shumov.tm.Commands.*;


public class Main {

    public static void main(String[] args) throws ParseException {
        boolean work = true;
        Scanner scr = new Scanner(System.in);
        Map<String, Task> tasks = new HashMap<>();
        Map<String, Project> projects = new HashMap<>();

        CreateProject createProject = new CreateProject(projects);
        ProjectList projectList = new ProjectList(projects);
        ProjectClear projectClear = new ProjectClear(tasks, projects);
        ProjectRemove projectRemove = new ProjectRemove(tasks, projects);
        CreateTask createTask = new CreateTask(tasks, projects);
        TaskList taskList = new TaskList(tasks);
        TaskClear taskClear = new TaskClear(tasks);
        TaskRemove taskRemove = new TaskRemove(tasks);
        Help help = new Help();

        System.out.println(WELCOME);
        System.out.println(PROJECT_ID);
        while (work) {
            String commandName = scr.nextLine();
            switch (commandName) {
                case HELP -> help.help();
                case PROJECT_CREATE -> createProject.createProject();
                case PROJECT_CLEAR -> projectClear.projectClear();
                case PROJECT_REMOVE -> projectRemove.projectRemove();
                case PROJECT_LIST -> projectList.projectList();
                case TASK_CREATE -> createTask.createTask();
                case TASK_CLEAR -> taskClear.taskClear();
                case TASK_REMOVE -> taskRemove.taskRemove();
                case TASK_LIST -> taskList.taskList();
                case EXIT -> work = false;
            }
        }
    }


}
