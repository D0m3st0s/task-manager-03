package ru.shumov.tm;

import ru.shumov.tm.entity.Task;

import java.util.Map;
import java.util.Scanner;

public class TaskClear {

    private Map<String, Task> tasks;

    public TaskClear(Map<String, Task> tasks) {
        this.tasks = tasks;
    }

    public void taskClear() {
        Scanner scrTaskCl = new Scanner(System.in);
        System.out.println(Commands.ALL_TASKS_WILL_BE_CLEARED);
        System.out.println(Commands.YES_NO);
        String answer = scrTaskCl.nextLine();

        if (answer.equals(Commands.YES)) {
            tasks.clear();
        }
        System.out.println(Commands.DONE);
    }
}
