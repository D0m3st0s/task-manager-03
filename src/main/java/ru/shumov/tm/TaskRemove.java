package ru.shumov.tm;

import ru.shumov.tm.entity.Task;

import java.util.Map;
import java.util.Scanner;

public class TaskRemove {

    private Map<String, Task> tasks;

    public TaskRemove(Map<String, Task> tasks) {
        this.tasks = tasks;
    }

    public void taskRemove() {
        Scanner scrTaskRe = new Scanner(System.in);
        System.out.println(Commands.ENTER_TASK_ID_FOR_REMOVING);
        String taskId = scrTaskRe.nextLine();
        tasks.remove(taskId);
        System.out.println(Commands.DONE);
    }
}
