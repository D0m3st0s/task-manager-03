package ru.shumov.tm;

import ru.shumov.tm.entity.Task;

import java.util.Collection;
import java.util.Map;
import java.util.Scanner;

public class TaskList {

    private Map<String, Task> tasks;

    public TaskList(Map<String, Task> tasks) {
        this.tasks = tasks;
    }

    public void taskList() {
        Scanner scrTaskList = new Scanner(System.in);
        if (tasks.isEmpty()) {
            System.out.println(Commands.TASKS_DO_NOT_EXIST);
        } else {
            System.out.println(Commands.SHOW_ALL_TASKS);
            System.out.println(Commands.YES_NO);
            String answer = scrTaskList.nextLine();

            if (answer.equals(Commands.YES)) {
                Collection<Task> values = tasks.values();
                for (Task task : values){
                    System.out.println(task);
                }
            }
            if (answer.equals(Commands.NO)) {
                System.out.println(Commands.ENTER_PROJECT_ID_FOR_SHOWING_TASKS);
                String projectId = scrTaskList.nextLine();
                Collection<Task> values = tasks.values();
                for (Task task: values){
                    if (task.getProjectId().equals(projectId)){
                        System.out.println(task);
                    }
                }
                System.out.println(tasks.get(projectId));
            }
        }
    }
}
